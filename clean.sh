#!/bin/sh
echo "Removing temporary files..."
rm log.txt
rm -r ./cmake
echo "Temporary files removed."

echo "Cleaning binaries..."
rm ./bin/SOCrate
echo "Binaries cleaned !"

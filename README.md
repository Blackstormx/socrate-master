# SOCrate Master - Maintenu par SkyzohKey.

## Bug fixs :
[**x**] **ClientListView** : Lors de l'ajout d'un client, les infos n’apparaissent pas dans le TreeView, à corriger !

## En cours de développement :
[x] **LoginWindow** : La fenêtre de connexion, avec le système de license.  
[x] **MainWindow**  : La fenêtre principale, avec la gestion des victimes, les paramètres, le dashboard, etc...

## Conventions
```vala
if (condition)
{
    int variable = 60;
    do_something (variable);
    return true;
}
else
{
    string variable = "Lorem ipsum";
    do_something_else (variable);
    return false;
}
```
## Compiler, nettoyer et commit :
Pour **compiler** il suffit d'utiliser le script **run.sh**
```
#!console
./build.sh      # Compile et lance le programme.
./build.sh -log # Compile et lance le programme en inscrivant les stdout dans le fichier `log.txt`.
```
Pour **nettoyer** il suffit d'utiliser le script **clean.sh**
```
#!console
./clean.sh
```
Pour **commit** il suffit d'utiliser le script **commit.sh**
```
#!console
./commit.sh       # Commit sans envoyer sur BitBucket.
./commit.sh -push # Commit + envoie sur BitBucket.
./commit.sh -pull # Récupérer les modifications via BitBucket sans commit.
```
**NB :** Utiliser `./commit.sh` pour les petits commit's et `./commit.sh -push` une fois une _grosse_ étape atteinte :100:

## Captures d'écran :
![LoginWindow](http://i.imgur.com/EfXrE1t.png)
![MainWindow](http://i.imgur.com/LFOSfZD.png)

## Idées :
_Pas encore d'idées_
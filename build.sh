#!/bin/sh
echo "Compiling project"

cd ./src
valac -o ../bin/SOCrate \
--vapidir "vapi" \
--pkg gmodule-2.0 --pkg gtk+-3.0 \
Main.vala \
ui/*.vala \

if [ "$1" = "-log" ]
then
    ../bin/SOCrate >> ../log.txt
else
    ../bin/SOCrate
fi

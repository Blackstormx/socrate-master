using SOCrate;
using Gtk;

namespace SOCrate
{
    class Master : Gtk.Application
    {
        private LoginWindow login_window;
        private MainWindow main_window;

        public Master ()
        {
            GLib.Object (
                application_id: "net.hack-free.socrate",
                flags: GLib.ApplicationFlags.HANDLES_OPEN
            );
        }

        private LoginWindow get_login_window ()
        {
            if (get_windows () == null)
                login_window = new LoginWindow (this);

            return login_window;
        }

        private MainWindow get_main_window ()
        {
            /*if (get_windows () == null)*/
            main_window = new MainWindow (this);

            return main_window;
        }

        protected override void activate ()
        {
            hold ();

            var login = get_login_window ();
            login.access_granted.connect (on_access_granted);
            login.access_error.connect (on_access_error);
            login.present ();

            release ();
        }

        private void on_access_granted (bool b)
        {
            if (b)
            {
                stdout.printf ("Access granted.\n\n");

                hold ();

                var main = get_main_window ();
                main.present ();
                login_window.close ();

                release ();
            }
            else
            {
                stdout.printf ("Access denied.\n\n");
            }
        }

        private void on_access_error (LoginError e)
        {
            stdout.printf ("Access denied - Error: %s\n", e.message);
        }
    }
}

    using SOCrate;
using Gtk;

namespace SOCrate
{
    public class Clients {
        public string ip;
        public int port;

        public Clients (string ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }
    }

    class MainWindow : Window
    {
        enum Column
        {
            IP,
            PORT
        }

        // Clients
        private ClientListView client_list_view;

        // CSS Files.
        private string[] css_files = { "css/main.css" };

        // Builder
        private Builder builder = new Builder ();

        // Widgets.
        private Box box;
        private Notebook tab_control;
        private Label label_copyright;

        private Image image_logo;
        private Image image_news;
        private Image image_network;

        private Box tab_dashboard_content;

        private Label label_clients_online;

        private Entry entry_settings_node_ip;
        private Entry entry_settings_node_port;
        private Entry entry_settings_node_key;
        private Button button_settings_save;

        public MainWindow (Gtk.Application application)
        {
            GLib.Object (application:application);

            this.title = "SOCrate - Master";
            this.set_default_size (800, 600);
            this.window_position = WindowPosition.CENTER;

            // Ugly way to load an icon, but it works :3
            try
            {
                this.icon = new Gdk.Pixbuf.from_file ("assets/icons/icon.png");
            }
            catch (Error e)
            {
                stderr.printf ("Could not load application icon: %s\n", e.message);
            }

            this.init_theme ();
            this.init_widgets ();
            this.init_signals ();
        }

        private void init_theme ()
        {
            // Init the themes.
            foreach (var file in this.css_files)
            {
                var provider = new CssProvider ();
                stdout.printf ("Testing %s...\n", file);

                try
                {
                    provider.load_from_path (file);
                    StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, STYLE_PROVIDER_PRIORITY_APPLICATION);
                }
                catch (GLib.Error e)
                {
                    stdout.printf ("%s -> Failure: %s\n", file, e.message);
                    continue;
                }

                stdout.printf ("%s -> OK\n", file);
            }
        }

        private void init_widgets ()
        {
            // TODO: Init the widgets here.
            try
            {
                this.builder.add_from_file ("ui/main.ui");
                this.builder.connect_signals (null);

                // Global widgets.
                this.box = builder.get_object ("box_main") as Box;
                this.tab_control = builder.get_object ("notebook_main") as Notebook;
                this.image_logo = builder.get_object ("image_logo") as Image;
                this.image_news = builder.get_object ("image_news") as Image;
                this.image_logo = builder.get_object ("image_logo") as Image;
                this.image_network = builder.get_object ("image_network") as Image;
                this.label_copyright = builder.get_object ("label_copyright") as Label;

                // Dashboard widgets.
                this.client_list_view = new ClientListView();
                this.client_list_view.show_all();
                this.tab_dashboard_content = builder.get_object ("tab_dashboard_content") as Box;
                this.tab_dashboard_content.pack_start(this.client_list_view, true, true, 0);
                this.init_client_list();

                // Clients widgets.
                this.label_clients_online = builder.get_object ("label_clients_online") as Label;

                // Settings widgets.
                this.entry_settings_node_ip = builder.get_object ("entry_settings_node_ip") as Entry;
                this.entry_settings_node_port = builder.get_object ("entry_settings_node_port") as Entry;
                this.entry_settings_node_key = builder.get_object ("entry_settings_node_key") as Entry;
                this.button_settings_save = builder.get_object ("button_settings_save") as Button;

                this.add (this.box);
            }
            catch (Error e)
            {
                stderr.printf ("Could not load UI: %s\n", e.message);
            }
        }

        private void init_signals ()
        {
            // TODO: Init the signals here.
            // lambda
            key_press_event.connect ((source, key) =>
            {
                if (key.keyval == Gdk.Key.F5)
                {
                    this.init_theme ();
                    stdout.printf ("Theme refreshed\n");
                    return true;
                }
                return false;
            });
        }

        private void init_client_list ()
        {
            /* DEBUG */
            Client shneck = new Client (
                "fr", // Lang.
                "Unknown", // ToxID.
                "84.18.67.69", // IP Address.
                "Linux of Bitchz", // Computer name.
                "elementaryOS (x64)", // Computer OS.
                true, // Is admin.
                "Idle" // Current state.
            );

            for (var i = 0; i < 50; i++)
            {
                this.client_list_view.add_client (shneck);
            }
        }
    }
}

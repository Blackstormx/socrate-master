using SOCrate;
using Gtk;

namespace SOCrate
{
    public class ClientListView : Gtk.TreeView
    {
        // Column types.
        private enum Columns
        {
            N_COLUMNS = 7,
            LANG,
            TOXID,
            IP_ADDRESS,
            COMPUTER_NAME,
            COMPUTER_OS,
            IS_ADMIN,
            STATE
        }

        // Signals.
        //public signal void line_activated (IClient client);

        // Widgets.
        private Gtk.ListStore list_store;
        Gtk.TreeViewColumn lang_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn toxid_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn ip_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn computer_name_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn computer_os_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn is_admin_column = new Gtk.TreeViewColumn ();
        Gtk.TreeViewColumn state_column = new Gtk.TreeViewColumn ();

        public ClientListView ()
        {
            this.list_store = new Gtk.ListStore (
                Columns.N_COLUMNS, // number of columns
                typeof (string),  // lang_column
                typeof (string), // toxid_column
                typeof (string), // ip_column
                typeof (string), // computer_name_column
                typeof (string), // computer_os_column
                typeof (string), // is_admin_column
                typeof (string)  // state_column
            );

            this.set_model (this.list_store); // Set the model to `this.list_store`

            // Define the columns properties.
            this.lang_column.title = "Lang";
            this.toxid_column.title = "ToxID";
            this.ip_column.title = "IP Address";
            this.computer_name_column.title = "Computer name";
            this.computer_os_column.title = "Operating system";
            this.is_admin_column.title = "Is admin ?";
            this.state_column.title = "Current state";

            // Add the columns.
            this.append_column (this.lang_column);
            this.append_column (this.toxid_column);
            this.append_column (this.ip_column);
            this.append_column (this.computer_name_column);
            this.append_column (this.computer_os_column);
            this.append_column (this.is_admin_column);
            this.append_column (this.state_column);

            this.expand = true; // Autoresize the columns
            this.set_rules_hint (true); // Make the row able to style even/odd.
            this.set_headers_visible (false); // Do not display the header.
        }

        public void add_client (Client client)
        {
            TreeIter iter;

            this.list_store.append (out iter);
            this.list_store.set (
                iter,
                0, client.lang,
                1, client.toxid,
                2, client.ip,
                3, client.computer_name,
                4, client.computer_os,
                5, client.is_admin,
                6, client.state
            );
        }
    }
}

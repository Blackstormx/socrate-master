using SOCrate;
using Gtk;

namespace SOCrate
{
    public class Client
    {
        public string lang = "en";
        public string toxid;
        public string ip;
        public string computer_name;
        public string computer_os;
        public string is_admin;
        public string state;

        public Client (
            string _lang, string _toxid, string _ip, string _computer_name,
            string _computer_os, bool _is_admin, string _state
        )
        {
            this.lang = _lang;
            this.toxid = _toxid;
            this.ip = _ip;
            this.computer_name = _computer_name;
            this.computer_os = _computer_os;
            this.is_admin = (_is_admin) ? "yes" : "no";
            this.state = _state;
        }
    }
}

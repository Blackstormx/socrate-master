using SOCrate;
using Gtk;

namespace SOCrate
{
    public errordomain LoginError
    {
        EMPTY_CREDENTIALS,
        BAD_CREDENTIALS,
        BAD_ACTIVATION_KEY,
        CANNOT_JOIN_AUTH
    }

    class LoginWindow : Window
    {
        // CSS Files.
        private string[] css_files = { "css/main.css" };

        // Signals.
        public signal void access_granted (bool is_granted);
        public signal void access_error (LoginError e);

        // Builder
        private Builder builder = new Builder ();

        // Widgets.
        private Box box;
        private Notebook notebook_main;

        private Entry entry_login_username;
        private Entry entry_login_userpass;
        private Button button_login;

        private Entry entry_register_username;
        private Entry entry_register_userpass;
        private Entry entry_register_userpass_confirmation;
        private Entry entry_register_usermail;
        private Entry entry_register_userhwid;
        private Button button_register;

        private Label label_help;

        public LoginWindow (Gtk.Application application)
        {
            GLib.Object (application:application);

            this.title = "SOCrate - Login";
            this.set_default_size (500, 400);
            this.window_position = WindowPosition.CENTER;

            try
            {
                this.icon = new Gdk.Pixbuf.from_file ("assets/icons/icon.png");
            }
            catch (Error e)
            {
                stderr.printf ("Could not load application icon: %s\n", e.message);
            }

            this.init_theme ();
            this.init_widgets ();
            this.init_signals ();
        }

        private void init_theme ()
        {
            // Init the themes.
            foreach (var file in this.css_files)
            {
                var provider = new CssProvider ();
                stdout.printf ("Testing %s...\n", file);

                try
                {
                    provider.load_from_path (file);
                    StyleContext.add_provider_for_screen (Gdk.Screen.get_default (), provider, STYLE_PROVIDER_PRIORITY_APPLICATION);
                }
                catch (GLib.Error e)
                {
                    stdout.printf ("%s -> Failure: %s\n", file, e.message);
                    continue;
                }

                stdout.printf("%s -> OK\n", file);
            }
        }

        private void init_widgets ()
        {
            // TODO: Init the widgets here.
            try
            {
                this.builder.add_from_file ("ui/login.ui");
                this.builder.connect_signals (null);

                // Global widgets.
                this.box = builder.get_object ("box_main") as Box;
                this.notebook_main = builder.get_object ("notebook_main") as Notebook;

                // Login widgets.
                this.entry_login_username = builder.get_object ("entry_login_username") as Entry;
                this.entry_login_userpass = builder.get_object ("entry_login_userpass") as Entry;
                this.button_login = builder.get_object ("button_login") as Button;

                // Register widgets.
                this.entry_register_username = builder.get_object ("entry_register_username") as Entry;
                this.entry_register_userpass = builder.get_object ("entry_register_userpass") as Entry;
                this.entry_register_userpass_confirmation = builder.get_object ("entry_register_userpass_confirmation") as Entry;
                this.entry_register_usermail = builder.get_object ("entry_register_usermail") as Entry;
                this.entry_register_userhwid = builder.get_object ("entry_register_userhwid") as Entry;
                this.button_register = builder.get_object ("button_register") as Button;

                // Help widgets.
                this.label_help = builder.get_object ("label_help") as Label;

                this.add (this.box);
            }
            catch (Error e)
            {
                stderr.printf ("Could not load UI: %s\n", e.message);
            }
        }

        private void init_signals ()
        {
            // TODO: Init the signals here.
            this.button_login.clicked.connect (this.on_button_login_clicked);

            // lambda
            key_press_event.connect ((source, key) =>
            {
                if (key.keyval == Gdk.Key.F5)
                {
                    this.init_theme ();
                    stdout.printf ("Theme refreshed\n");
                    return true;
                }

                return false;
            });
        }

        private void on_button_login_clicked (Button source)
        {
            stdout.printf("Login...\n");
            try
            {
                if (this.login (this.entry_login_username, this.entry_login_userpass))
                    this.access_granted (true);
            }
            catch (LoginError e)
            {
                this.access_granted (false);
                this.access_error (e);
            }
        }

        private bool login (Entry username, Entry userpass) throws LoginError
        {
            if (username.text == "" || userpass.text == "")
                throw new LoginError.EMPTY_CREDENTIALS ("You must enter non-empty credentials.");

            if (username.text == "SkyzohKey" || username.text == "Ogromny")
            {
                if (userpass.text == "Test123")
                    return true;
                else
                    throw new LoginError.BAD_CREDENTIALS("Invalid password.");
            }
            else
                throw new LoginError.BAD_CREDENTIALS("Invalid username.");
        }
    }
}

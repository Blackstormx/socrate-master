using SOCrate;
using Gtk;

namespace SOCrate
{
    public class Main : GLib.Object
    {
        public static string version = "0.0.3"; // Don't work e.e

        public static int main (string[] args)
        {
            stdout.printf("\n~ SOCrate (version %s) ~\n", version);

            // TODO: Make use of i18n or GetText:
            /*GLib.Intl.setlocale(GLib.LocaleCategory.MESSAGES, "");
            GLib.Intl.textdomain(Config.GETTEXT_PACKAGE);
            GLib.Intl.bind_textdomain_codeset(Config.GETTEXT_PACKAGE, "utf-8");

            GLib.OptionContext option_context = new GLib.OptionContext("");
            option_context.set_help_enabled(true);

            try { option_context.parse(ref args); }
            catch (GLib.OptionError e)
            {
                stdout.printf(_("error: %s\n"), e.message);
                return -1;
            }*/

            return new Master().run(args);
        }
    }
}
